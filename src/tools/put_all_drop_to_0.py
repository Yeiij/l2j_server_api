import os
import cowsay
from bs4 import (
    BeautifulSoup as bs,
    Comment,
)


def get_xmls_list(path: str) -> list:
    """
    Read all xml files in the given path folder,
    returns those xml's names as a list
    :param path:
    :type path: str
    :return:
    :rtype: list
    """
    data: list = []
    try:
        for file in os.listdir(path):
            if file.endswith(".xml"):
                data.append(file.strip())
    except Exception as e:
        print(f'Smething goes wrong: {e}')
    finally:
        return data


def main():
    """
    Main function that executes the whole logic
    :return: none
    :rtype: None
    """
    # vars definitions
    xml_path: str = 'D:/IdeaProjects/L2JServer' \
                    '/l2j-server-datapack/src/main/resources/data/stats/npcs'
    output_path: str = './output'

    # run functions
    cowsay.daemon('Scraping, please wait...')
    xml_files: list = get_xmls_list(xml_path)

    for xml in xml_files:

        print(f'Reading {xml} file')
        soup = bs(open(f'{xml_path}/{xml}'), 'lxml')

        group_els = soup.findAll('group')
        item_els = soup.findAll('item')

        for group_el in group_els:
            group_change = group_el.attrs['chance']
            group_el.attrs['chance'] = 0
            group_el.append(Comment(f' group default chance: {group_change} '))

        for item_el in item_els:
            item_chance = item_el.attrs['chance']
            item_el.attrs['chance'] = 0
            item_el.append(Comment(f' item default chance: {item_chance} '))

        if len(group_els) > 0 or len(item_els) > 0:
            print(f'Writing {xml} file')
            with open(f'{output_path}/{xml}', 'w', encoding='utf-8') as f:
                f.write(str(soup.prettify()))
                f.close()

    cowsay.dragon(f'Finished: {output_path}')


if __name__ == '__main__':
    main()
