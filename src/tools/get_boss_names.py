import json


def get_boss_name() -> dict:
    """
    Load the data from the json file that contains all the boss names and
    return the content
    :return:
    :rtype: dict
    """
    data = json
    try:
        with open('./tools/output/rb_data.json', 'r', encoding='utf-8') as f:
            data = json.load(f)
            f.close()
    except Exception as e:
        print(f'Smething goes wrong: {e}')
    finally:
        return data
