import json
import os

import cowsay
from bs4 import BeautifulSoup as bs


def get_input_data(file: str, path: str) -> list:
    """
    Load the content from the file that contains RaidBoss ID's and transform
    them into a list
    :param file:
    :type file: str
    :param path:
    :type path: str
    :return:
    :rtype: list
    """
    data: list = []
    try:
        with open(f'{path}/{file}', 'r', encoding='utf-8') as f:
            lines = f.readlines()
            for line in lines:
                data.append(str(line).strip())
            f.close()
    except Exception as e:
        print(f'Smething goes wrong: {e}')
    finally:
        return data


def get_filtered_xmls_list(path: str, data_source: list) -> list:
    """
    Read all xml files in the given path folder, filters the xml's that we need
    and return those xml's names as a list
    :param path:
    :type path: str
    :param data_source:
    :type data_source: list
    :return:
    :rtype: list
    """
    data: list = []
    try:
        for file in os.listdir(path):
            if file.endswith(".xml"):
                file_name = file.strip()
                file_name = file_name.replace('.xml', '')
                file_name = file_name.split('-')
                if [i in data_source for i in file_name]:
                    data.append(file.strip())
    except Exception as e:
        print(f'Smething goes wrong: {e}')
    finally:
        return data


def get_filtered_npcs(items: list, path: str, ids: list) -> dict:
    """
    Filters all elements in each xml using the RaidBoss ID's and return a
    dictionary that contains the data
    :param items:
    :type items: list
    :param path:
    :type path: str
    :param ids:
    :type ids: list
    :return:
    :rtype: dict
    """
    data = {}
    try:
        for item in items:
            soup = bs(open(f'{path}/{item}'), 'lxml')
            elements = soup.findAll('npc')

            for element in elements:
                npc_id = element.attrs.get('id')
                npc_name = element.attrs.get('name')

                if npc_id in ids:
                    data.update({f'{npc_id}': npc_name})

    except Exception as e:
        print(f'Smething goes wrong: {e}')
    finally:
        return data


def store_npc_data(data: dict, file: str, path: str) -> bool:
    """
    Save the data to a file in JSON format
    :param data:
    :type data: dict
    :param file:
    :type file: str
    :param path:
    :type path: str
    :return: returns a boolean that indicates whether or not the function
    was successfully executed
    :rtype: bool
    """
    done = False
    try:
        with open(f'{path}/{file}', 'w', encoding='utf-8') as f:
            json.dump(
                data,
                f,
                ensure_ascii=False,
                indent=4,
                separators=(',', ': '),
                sort_keys=True
            )
            done = True
    except Exception as e:
        print(f'Smething goes wrong: {e}')
    finally:
        return done


def main():
    """
    Main function that executes the whole logic
    :return: none
    :rtype: None
    """
    # vars definitions
    input_path: str = './input'
    input_file: str = 'input_ids.txt'
    output_path: str = './output'
    output_file: str = 'rb_data.json'
    xml_path: str = 'D:/IdeaProjects/L2JServer' \
                    '/l2j-server-datapack/src/main/resources/data/stats/npcs'

    # run functions
    cowsay.daemon('Scraping, please wait...')
    data_source: list = get_input_data(input_file, input_path)
    xml_files: list = get_filtered_xmls_list(xml_path, data_source)
    data_npc: dict = get_filtered_npcs(xml_files, xml_path, data_source)
    done: bool = store_npc_data(data_npc, output_file, output_path)

    if not done:
        cowsay.kitty('Woops, something failed at the end...')
        exit()

    cowsay.dragon(f'Finished: {output_path}/{output_file}')


if __name__ == '__main__':
    main()
