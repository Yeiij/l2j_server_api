from django.db import models


class Spawnlist(models.Model):
    location = models.CharField(max_length=40)
    count = models.PositiveIntegerField()
    npc_templateid = models.PositiveSmallIntegerField(primary_key=True)
    locx = models.IntegerField()
    locy = models.IntegerField()
    locz = models.IntegerField()
    randomx = models.IntegerField()
    randomy = models.IntegerField()
    heading = models.IntegerField()
    respawn_delay = models.IntegerField()
    respawn_random = models.IntegerField()
    loc_id = models.IntegerField()
    periodofday = models.PositiveIntegerField(db_column='periodOfDay')

    class Meta:
        managed = False
        db_table = 'spawnlist'
        verbose_name = 'spawnlist'
        verbose_name_plural = 'Spawnlist'

        unique_together = (('npc_templateid', 'locx', 'locy', 'locz'),)
