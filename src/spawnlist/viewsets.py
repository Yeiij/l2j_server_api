from rest_framework.viewsets import ReadOnlyModelViewSet

from .filters import SpawnlistFilter
from .models import Spawnlist
from .serializers import SpawnlistSerializer


class SpawnlistViewSet(ReadOnlyModelViewSet):
    allowed_methods = ['GET', ]
    http_method_names = ['get', ]
    queryset = Spawnlist.objects.using('l2jgs').all()
    serializer_class = SpawnlistSerializer
    filterset_class = SpawnlistFilter
