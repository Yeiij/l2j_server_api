from django_filters import rest_framework as filters

from .models import Spawnlist


class SpawnlistFilter(filters.FilterSet):
    class Meta:
        model = Spawnlist
        fields = {
            'npc_templateid': ['exact', ],
        }
