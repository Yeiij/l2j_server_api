from rest_framework.serializers import (
    CharField,
    IntegerField,
    ModelSerializer,
)

from .models import Spawnlist


class SpawnlistSerializer(ModelSerializer):
    location = CharField()
    npc_templateid = IntegerField()
    locx = IntegerField()
    locy = IntegerField()
    locz = IntegerField()
    heading = IntegerField()
    respawn_delay = IntegerField()

    class Meta:
        model = Spawnlist
        fields = (
            'location',
            'npc_templateid',
            'locx',
            'locy',
            'locz',
            'heading',
            'respawn_delay'
        )
        read_only_fields = (
            'location',
            'npc_templateid',
            'locx',
            'locy',
            'locz',
            'heading',
            'respawn_delay'
        )
