from django.contrib import admin

from .models import Spawnlist


@admin.register(Spawnlist)
class SpawnlistAdmin(admin.ModelAdmin):
    list_display = (
        'location',
        'count',
        'npc_templateid',
        'locx',
        'locy',
        'locz',
        'randomx',
        'randomy',
        'heading',
        'respawn_delay',
        'respawn_random',
        'loc_id',
        'periodofday',
    )
