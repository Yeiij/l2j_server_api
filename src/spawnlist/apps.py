from django.apps import AppConfig


class SpawnlistConfig(AppConfig):
    name = 'spawnlist'
