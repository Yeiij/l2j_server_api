from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer

from .models import GrandbossData

from tools.get_boss_names import get_boss_name


class GrandbossDataSerializer(ModelSerializer):
    name = SerializerMethodField()
    status = SerializerMethodField()

    class Meta:
        model = GrandbossData
        fields = [
            'name',
            'status'
        ]
        read_only_fields = fields

    def get_name(self, obj):
        return get_boss_name()[f'{obj.boss_id}']

    def get_status(self, obj):
        return GrandbossData.STATUS_CHOICES[obj.status]
