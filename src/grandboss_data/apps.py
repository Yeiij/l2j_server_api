from django.apps import AppConfig


class GrandBossDataConfig(AppConfig):
    name = 'grandboss_data'
