from django.contrib import admin

from .models import GrandbossData


@admin.register(GrandbossData)
class GrandbossDataAdmin(admin.ModelAdmin):
    list_display = (
        'boss_id',
        'loc_x',
        'loc_y',
        'loc_z',
        'heading',
        'respawn_time',
        'currentHP',
        'currentMP',
        'status',
    )
