from django.db import models


class GrandbossData(models.Model):
    STATUS_CHOICES = (
        'alive',
        'waiting',
        'fighting',
        'dead'
    )

    boss_id = models.PositiveSmallIntegerField(primary_key=True)
    loc_x = models.IntegerField()
    loc_y = models.IntegerField()
    loc_z = models.IntegerField()
    heading = models.IntegerField()
    respawn_time = models.PositiveBigIntegerField()
    currentHP = models.DecimalField(max_digits=30, decimal_places=15)
    currentMP = models.DecimalField(max_digits=30, decimal_places=15)
    status = models.PositiveIntegerField()

    class Meta:
        managed = False
        db_table = 'grandboss_data'
        verbose_name = 'grandboss'
        verbose_name_plural = 'Grandboss'
