from rest_framework.viewsets import ReadOnlyModelViewSet

from .filters import GrandbossDataFilter
from .models import GrandbossData
from .serializers import GrandbossDataSerializer


class GrandbossDataViewSet(ReadOnlyModelViewSet):
    allowed_methods = ['GET', ]
    http_method_names = ['get', ]
    queryset = GrandbossData.objects.using('l2jgs').all()
    serializer_class = GrandbossDataSerializer
    filterset_class = GrandbossDataFilter
