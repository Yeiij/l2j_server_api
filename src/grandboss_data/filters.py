from django_filters import rest_framework as filters

from .models import GrandbossData


class GrandbossDataFilter(filters.FilterSet):
    class Meta:
        model = GrandbossData
        fields = {
            'boss_id': ['exact', ],
        }
