from rest_framework.viewsets import ReadOnlyModelViewSet

from .filters import RaidbossSpawnlistFilter
from .models import RaidbossSpawnlist
from .serializers import RaidbossSpawnlistSerializer


class RaidbossSpawnlistViewSet(ReadOnlyModelViewSet):
    allowed_methods = ['GET', ]
    http_method_names = ['get', ]
    queryset = RaidbossSpawnlist.objects.using('l2jgs').all()
    serializer_class = RaidbossSpawnlistSerializer
    filterset_class = RaidbossSpawnlistFilter
