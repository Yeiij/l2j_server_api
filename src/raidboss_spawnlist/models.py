from django.db import models


class RaidbossSpawnlist(models.Model):
    boss_id = models.PositiveSmallIntegerField(primary_key=True)
    amount = models.PositiveIntegerField()
    loc_x = models.IntegerField()
    loc_y = models.IntegerField()
    loc_z = models.IntegerField()
    heading = models.IntegerField()
    respawn_delay = models.PositiveIntegerField()
    respawn_random = models.PositiveIntegerField()
    respawn_time = models.PositiveBigIntegerField()
    currentHp = models.DecimalField(max_digits=8, decimal_places=0, blank=True,
                                    null=True)
    currentMp = models.DecimalField(max_digits=8, decimal_places=0, blank=True,
                                    null=True)

    class Meta:
        managed = False
        db_table = 'raidboss_spawnlist'
        verbose_name = 'raidboss'
        verbose_name_plural = 'Raidboss'
