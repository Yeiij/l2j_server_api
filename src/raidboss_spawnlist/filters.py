from django_filters import rest_framework as filters

from .models import RaidbossSpawnlist


class RaidbossSpawnlistFilter(filters.FilterSet):
    class Meta:
        model = RaidbossSpawnlist
        fields = {
            'boss_id': ['exact', ],
        }
