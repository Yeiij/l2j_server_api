from django.contrib import admin

from .models import RaidbossSpawnlist


@admin.register(RaidbossSpawnlist)
class RaidbossSpawnlistAdmin(admin.ModelAdmin):
    list_display = (
        'boss_id',
        'amount',
        'loc_x',
        'loc_y',
        'loc_z',
        'heading',
        'respawn_delay',
        'respawn_random',
        'respawn_time',
        'currentHp',
        'currentMp',
    )
