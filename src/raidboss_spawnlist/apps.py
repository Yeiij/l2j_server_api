from django.apps import AppConfig


class RaidbossSpawnListConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'raidboss_spawnlist'
