from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer

from .models import RaidbossSpawnlist

from tools.get_boss_names import get_boss_name


class RaidbossSpawnlistSerializer(ModelSerializer):
    name = SerializerMethodField()
    status = SerializerMethodField()

    class Meta:
        model = RaidbossSpawnlist
        fields = [
            'name',
            'status'
        ]
        read_only_fields = fields

    def get_name(self, obj):
        return get_boss_name()[f'{obj.boss_id}']

    def get_status(self, obj):
        return 'alive' if obj.respawn_time == 0 else 'dead'
