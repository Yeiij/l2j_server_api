from django.shortcuts import render


def home(request) -> render:
    ctx = {}
    return render(request, 'home/home.html', ctx)
