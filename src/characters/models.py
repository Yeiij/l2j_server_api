from django.db import models


class Character(models.Model):
    CLASS_CHOICES = (
        'fighter',
        'warrior',
        'gladiator',
        'warlord',
        'knight',
        'paladin',
        'darkAvenger',
        'rogue',
        'treasureHunter',
        'hawkeye',
        'mage',
        'wizard',
        'sorceror',
        'necromancer',
        'warlock',
        'cleric',
        'bishop',
        'prophet',
        'elvenFighter',
        'elvenKnight',
        'templeKnight',
        'swordSinger',
        'elvenScout',
        'plainsWalker',
        'silverRanger',
        'elvenMage',
        'elvenWizard',
        'spellsinger',
        'elementalSummoner',
        'oracle',
        'elder',
        'darkFighter',
        'palusKnight',
        'shillienKnight',
        'bladedancer',
        'assassin',
        'abyssWalker',
        'phantomRanger',
        'darkMage',
        'darkWizard',
        'spellhowler',
        'phantomSummoner',
        'shillienOracle',
        'shillenElder',
        'orcFighter',
        'orcRaider',
        'destroyer',
        'orcMonk',
        'tyrant',
        'orcMage',
        'orcShaman',
        'overlord',
        'warcryer',
        'dwarvenFighter',
        'scavenger',
        'bountyHunter',
        'artisan',
        'warsmith',
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        'duelist',
        'dreadnought',
        'phoenixKnight',
        'hellKnight',
        'sagittarius',
        'adventurer',
        'archmage',
        'soultaker',
        'arcanaLord',
        'cardinal',
        'hierophant',
        'evaTemplar',
        'swordMuse',
        'windRider',
        'moonlightSentinel',
        'mysticMuse',
        'elementalMaster',
        'evaSaint',
        'shillienTemplar',
        'spectralDancer',
        'ghostHunter',
        'ghostSentinel',
        'stormScreamer',
        'spectralMaster',
        'shillienSaint',
        'titan',
        'grandKhavatari',
        'dominator',
        'doomcryer',
        'fortuneSeeker',
        'maestro',
        'dummyEntry31',
        'dummyEntry32',
        'dummyEntry33',
        'dummyEntry34',
        'maleSoldier',
        'femaleSoldier',
        'trooper',
        'warder',
        'berserker',
        'maleSoulbreaker',
        'femaleSoulbreaker',
        'arbalester',
        'doombringer',
        'maleSoulhound',
        'femaleSoulhound',
        'trickster',
        'inspector',
        'judicator'
    )

    RACE_CHOICES = (
        'human',
        'elf',
        'dark elf',
        'orc',
        'dwarf',
        'kamael',
        'animal',
        'beast',
        'bug',
        'castle guard',
        'construct',
        'demonic',
        'divine',
        'dragon',
        'elemental',
        'etc',
        'fairy',
        'giant',
        'humanoid',
        'mercenary',
        None,
        'plant',
        'siege weapon',
        'undead'
    )

    SEX_CHOICES = (
        'male',
        'female',
        'other'
    )

    account_name = models.CharField(max_length=45, blank=True, null=True)
    charId = models.PositiveIntegerField(primary_key=True)
    char_name = models.CharField(max_length=35)
    level = models.PositiveIntegerField(blank=True, null=True)
    maxHp = models.PositiveIntegerField(blank=True, null=True)
    curHp = models.PositiveIntegerField(blank=True, null=True)
    maxCp = models.PositiveIntegerField(blank=True, null=True)
    curCp = models.PositiveIntegerField(blank=True, null=True)
    maxMp = models.PositiveIntegerField(blank=True, null=True)
    curMp = models.PositiveIntegerField(blank=True, null=True)
    face = models.PositiveIntegerField(blank=True, null=True)
    hairStyle = models.PositiveIntegerField(blank=True, null=True)
    hairColor = models.PositiveIntegerField(blank=True, null=True)
    sex = models.PositiveIntegerField(blank=True, null=True)
    heading = models.IntegerField(blank=True, null=True)
    x = models.IntegerField(blank=True, null=True)
    y = models.IntegerField(blank=True, null=True)
    z = models.IntegerField(blank=True, null=True)
    exp = models.PositiveBigIntegerField(blank=True, null=True)
    expBeforeDeath = models.PositiveBigIntegerField(blank=True, null=True)
    sp = models.PositiveIntegerField()
    karma = models.PositiveIntegerField(blank=True, null=True)
    fame = models.PositiveIntegerField()
    pvpkills = models.PositiveSmallIntegerField(blank=True, null=True)
    pkkills = models.PositiveSmallIntegerField(blank=True, null=True)
    clanid = models.PositiveIntegerField(blank=True, null=True)
    race = models.PositiveIntegerField(blank=True, null=True)
    classid = models.PositiveIntegerField(blank=True, null=True)
    base_class = models.PositiveIntegerField()
    transform_id = models.PositiveSmallIntegerField()
    deletetime = models.PositiveBigIntegerField()
    cancraft = models.PositiveIntegerField(blank=True, null=True)
    title = models.CharField(max_length=21, blank=True, null=True)
    title_color = models.PositiveIntegerField()
    accesslevel = models.IntegerField(blank=True, null=True)
    online = models.PositiveIntegerField(blank=True, null=True)
    onlinetime = models.IntegerField(blank=True, null=True)
    char_slot = models.PositiveIntegerField(blank=True, null=True)
    newbie = models.PositiveIntegerField(blank=True, null=True)
    lastAccess = models.PositiveBigIntegerField()
    clan_privs = models.PositiveIntegerField(blank=True, null=True)
    wantspeace = models.PositiveIntegerField(blank=True, null=True)
    isin7sdungeon = models.PositiveIntegerField()
    power_grade = models.PositiveIntegerField(blank=True, null=True)
    nobless = models.PositiveIntegerField()
    subpledge = models.SmallIntegerField()
    lvl_joined_academy = models.PositiveIntegerField()
    apprentice = models.PositiveIntegerField()
    sponsor = models.PositiveIntegerField()
    clan_join_expiry_time = models.PositiveBigIntegerField()
    clan_create_expiry_time = models.PositiveBigIntegerField()
    death_penalty_level = models.PositiveSmallIntegerField()
    bookmarkslot = models.PositiveSmallIntegerField()
    vitality_points = models.PositiveSmallIntegerField()
    createDate = models.DateTimeField()
    language = models.CharField(max_length=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'characters'
        verbose_name = "characters"
        verbose_name_plural = 'Characters'
