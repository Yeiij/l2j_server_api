from django.db.models import ObjectDoesNotExist
from rest_framework.serializers import (
    CharField,
    ModelSerializer,
    SerializerMethodField
)

from .models import Character
from clan_data.models import ClanData
from clan_data.serializers import ClanDataSerializer


class CharacterSerializer(ModelSerializer):
    classid = SerializerMethodField()
    nobless = SerializerMethodField()
    race = SerializerMethodField()
    sex = SerializerMethodField()
    clan = SerializerMethodField()

    fame = CharField()
    karma = CharField()
    level = CharField()
    pvpkills = CharField()
    pkkills = CharField()
    clanid = CharField()

    class Meta:
        model = Character
        fields = [
            'char_name',
            'classid',
            'fame',
            'karma',
            'level',
            'nobless',
            'pvpkills',
            'pkkills',
            'race',
            'sex',
            'clanid',
            'clan'
        ]
        read_only_fields = fields

    def get_race(self, obj):
        r = obj.race
        try:
            r = Character.RACE_CHOICES[obj.race]
        except Exception as e:
            print(f'Error: {e}')
        finally:
            return r

    def get_nobless(self, obj):
        return 'no' if obj.nobless == 0 else 'yes'

    def get_sex(self, obj):
        r = obj.sex
        try:
            r = Character.SEX_CHOICES[obj.sex]
        except Exception as e:
            print(f'Error: {e}')
        finally:
            return r

    def get_classid(self, obj):
        r = obj.classid
        try:
            r = Character.CLASS_CHOICES[obj.classid]
        except Exception as e:
            print(f'Error: {e}')
        finally:
            return '' if r is None else r

    def get_clan(self, obj):
        serializer = None
        try:
            data = ClanData.objects.using('l2jgs').get(pk=obj.clanid)
            serializer = ClanDataSerializer(data)
        except ObjectDoesNotExist:
            serializer = ClanDataSerializer()
        finally:
            return None if len(serializer.data) == 0 else serializer.data
