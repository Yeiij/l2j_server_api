from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet

from .filters import CharacterFilter
from .models import Character
from .serializers import CharacterSerializer


class CharacterViewSet(ReadOnlyModelViewSet):
    allowed_methods = ['GET', ]
    http_method_names = ['get', ]
    queryset = Character.objects.using('l2jgs').all()
    serializer_class = CharacterSerializer
    filterset_class = CharacterFilter

    @action(detail=False, name='TOP-PK', url_path='top-pk')
    def top_pk(self, request):
        res = self.get_queryset().filter(
            pkkills__gt=0
        ).order_by('-pkkills')[:10]
        serializer = self.get_serializer_class()(res, many=True)
        return Response(serializer.data)

    @action(detail=False, name='TOP-PvP', url_path='top-pvp')
    def top_pvp(self, request):
        res = self.get_queryset().filter(
            pvpkills__gt=0
        ).order_by('-pvpkills')[:10]
        serializer = self.get_serializer_class()(res, many=True)
        return Response(serializer.data)
