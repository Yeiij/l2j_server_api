from rest_framework import routers

from characters.viewsets import CharacterViewSet
from clan_data.viewsets import ClanDataViewSet
from grandboss_data.viewsets import GrandbossDataViewSet
from raidboss_spawnlist.viewsets import RaidbossSpawnlistViewSet
from spawnlist.viewsets import SpawnlistViewSet

router = routers.DefaultRouter()
router.register('characters', CharacterViewSet, basename='characters')
router.register('clan_data', ClanDataViewSet, basename='clan_data')
router.register(
    'grandboss_data',
    GrandbossDataViewSet,
    basename='grandboss_data'
)
router.register(
    'raidboss_spawnlist',
    RaidbossSpawnlistViewSet,
    basename='raidboss_spawnlist'
)
router.register('spawnlist', SpawnlistViewSet, basename='spawnlist')
