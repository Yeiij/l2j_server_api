import os
import sys
from pathlib import Path

import environ

# ENVIRON CONFIGURATION -------------------------------------------------------
env = environ.Env()
env.read_env()
DEVELOPMENT = env.bool('DEVELOPMENT', default=False)

# DJANGO CONFIGURATION --------------------------------------------------------
BASE_DIR = Path(__file__).resolve().parent.parent

sys.path.insert(0, os.path.join(BASE_DIR, 'apps'))

SECRET_KEY = env.str(
    'SECRET_KEY',
    default='l2jserverctt&7s*kgonp)z)x_ii12p$!&#m+=or$6+tpkclx!gtmmh%br_'
)

DEBUG = env.bool('DEBUG', default=False)

ALLOWED_HOSTS = env.tuple('ALLOWED_HOSTS', default='*')

WSGI_APPLICATION = 'l2j_server_api.wsgi.application'

# APPS ------------------------------------------------------------------------
# - DEFAULT INSTALLED APPS
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]
# - THIRD PARTY APPS
INSTALLED_APPS += [
    'crispy_forms',
    'corsheaders',
    'django_filters',
    'rest_framework',
]
# - DEV APPS
if DEVELOPMENT:
    INSTALLED_APPS += [
        'debug_toolbar',
        'django_extensions',
        'django_seed',
    ]
# - PROJECT APPS
INSTALLED_APPS += [
    'characters',
    'clan_data',
    'core',
    'grandboss_data',
    'home',
    'raidboss_spawnlist',
    'spawnlist',
]

# MIDDLEWARE ------------------------------------------------------------------
# - DEFAULT MIDDLEWARE
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]
# - THIRD PARTY MIDDLEWARE
MIDDLEWARE += [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
]
# - DEV MIDDLEWARE
if DEVELOPMENT:
    MIDDLEWARE += [
        'debug_toolbar.middleware.DebugToolbarMiddleware',
    ]
# - PROJECT MIDDLEWARE
MIDDLEWARE += []

# DATABASE CONFIGURATION ------------------------------------------------------
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    },
    'l2jgs': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': env.str('NAME', default='l2jgs'),
        'USER': env.str('USER', default='l2j'),
        'PASSWORD': env.str('PASSWORD', default='l2jserver2019'),
        'HOST': env.str('HOST', default='localhost'),
        'PORT': env.str('PORT', default='3306'),
    }
}

# SECURITY CONFIGURATION ------------------------------------------------------
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation'
                '.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation'
                '.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation'
                '.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation'
                '.NumericPasswordValidator',
    },
]

# ENCODING CONFIGURATION ------------------------------------------------------
LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# STATIC FILES CONFIGURATION --------------------------------------------------
ROOT_URLCONF = 'l2j_server_api.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

STATIC_URL = '/static/'

# DJANGO DEBUG TOOLBAR CONFIGURATION ------------------------------------------
if DEVELOPMENT:
    INTERNAL_IPS = [
        '127.0.0.1',
        'localhost',
    ]

# DJANGO CORST HEADERS CONFIGURATION ------------------------------------------

CORS_ORIGIN_ALLOW_ALL = False
CORS_ORIGIN_WHITELIST = [
    "http://localhost",
    "http://localhost:8000",
    "http://localhost:8080",
    "http://127.0.0.1",
    "http://127.0.0.1:8000",
    "http://127.0.0.1:8080",
]
CORS_ALLOWED_ORIGINS = [
    "http://localhost",
    "http://localhost:8000",
    "http://localhost:8080",
    "http://127.0.0.1",
    "http://127.0.0.1:8000",
    "http://127.0.0.1:8080",
]
CORS_ALLOW_METHODS = [
    'GET',
]
CORS_ALLOW_HEADERS = [
    'accept',
    'accept-encoding',
    'authorization',
    'cache-control',
    'content-type',
    'dnt',
    'origin',
    'user-agent',
    'x-csrftoken',
    'x-requested-with',
    'access-control-allow-headers'
]

# DJANGO REST FRAMEWORK CONFIGURATION -----------------------------------------
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    ),
    'DEFAULT_FILTER_BACKENDS': (
        'django_filters.rest_framework.DjangoFilterBackend',
    ),
    'DEFAULT_PARSER_CLASSES': (
        'rest_framework.parsers.JSONParser',
        'rest_framework.parsers.FormParser',
        'rest_framework.parsers.MultiPartParser',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
    ),

    'DEFAULT_CONTENT_NEGOTIATION_CLASS':
        'rest_framework.negotiation.DefaultContentNegotiation',
    'DEFAULT_METADATA_CLASS': 'rest_framework.metadata.SimpleMetadata',
    'DEFAULT_SCHEMA_CLASS': 'rest_framework.schemas.coreapi.AutoSchema',
    'DEFAULT_PAGINATION_CLASS':
        'rest_framework.pagination.LimitOffsetPagination'
}
