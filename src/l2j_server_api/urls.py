import debug_toolbar
from django.contrib import admin
from django.urls import (
    include,
    path,
)

from .routers import router
from .settings import DEVELOPMENT

urlpatterns = [
    # - DEFAULT URLS
    path('admin/', admin.site.urls),

    # - THIRD PARTY URLS
    path('api-auth/', include('rest_framework.urls')),

    # - PROJECT URLS
    path('', include('home.urls')),
    path('api/', include(router.urls)),
]

if DEVELOPMENT:
    urlpatterns += [
        # - DEV URLS
        path('__debug__/', include(debug_toolbar.urls)),
    ]
