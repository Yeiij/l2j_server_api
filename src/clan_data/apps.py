from django.apps import AppConfig


class ClanDataConfig(AppConfig):
    name = 'clan_data'
