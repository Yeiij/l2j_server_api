from django.contrib import admin

from .models import ClanData


@admin.register(ClanData)
class ClanDataAdmin(admin.ModelAdmin):
    list_display = (
        'clan_id',
        'clan_name',
        'clan_level',
        'reputation_score',
        'hasCastle',
        'blood_alliance_count',
        'blood_oath_count',
        'ally_id',
        'ally_name',
        'leader_id',
        'crest_id',
        'crest_large_id',
        'ally_crest_id',
        'auction_bid_at',
        'ally_penalty_expiry_time',
        'ally_penalty_type',
        'char_penalty_expiry_time',
        'dissolving_expiry_time',
        'new_leader_id'
    )
