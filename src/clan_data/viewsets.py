from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet

from .filters import ClanDataFilter
from .models import ClanData
from .serializers import ClanDataSerializer


class ClanDataViewSet(ReadOnlyModelViewSet):
    allowed_methods = ['GET', ]
    http_method_names = ['get', ]
    queryset = ClanData.objects.using('l2jgs').all()
    serializer_class = ClanDataSerializer
    filterset_class = ClanDataFilter

    @action(detail=False, name='TOP-LEVEL', url_path='top-level')
    def top_level(self, request):
        res = self.get_queryset().order_by(
            '-clan_level',
            '-reputation_score'
        )[:10]
        serializer = self.get_serializer_class()(res, many=True)
        return Response(serializer.data)
