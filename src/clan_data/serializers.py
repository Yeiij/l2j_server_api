from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer

from .models import ClanData


class ClanDataSerializer(ModelSerializer):
    ally_name = SerializerMethodField()
    clan_level = SerializerMethodField()
    hasCastle = SerializerMethodField()
    reputation_score = SerializerMethodField()

    class Meta:
        model = ClanData
        fields = [
            'ally_name',
            'clan_level',
            'clan_name',
            'hasCastle',
            'reputation_score'
        ]
        read_only_fields = fields

    def get_ally_name(self, obj):
        return '' if obj.ally_name is None else obj.ally_name

    def get_clan_level(self, obj):
        return str(obj.clan_level) if obj.clan_level is not None else None

    def get_hasCastle(self, obj):
        return 'no' if obj.hasCastle == 0 else 'yes'

    def get_reputation_score(self, obj):
        return str(
            obj.reputation_score
        ) if obj.reputation_score is not None else None
