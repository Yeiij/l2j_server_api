from django.db import models


class ClanData(models.Model):
    clan_id = models.IntegerField(primary_key=True)
    clan_name = models.CharField(max_length=45, blank=True, null=True)
    clan_level = models.IntegerField(blank=True, null=True)
    reputation_score = models.IntegerField()
    hasCastle = models.IntegerField(blank=True, null=True)
    blood_alliance_count = models.PositiveSmallIntegerField()
    blood_oath_count = models.PositiveSmallIntegerField()
    ally_id = models.IntegerField(blank=True, null=True)
    ally_name = models.CharField(max_length=45, blank=True, null=True)
    leader_id = models.IntegerField(blank=True, null=True)
    crest_id = models.IntegerField(blank=True, null=True)
    crest_large_id = models.IntegerField(blank=True, null=True)
    ally_crest_id = models.IntegerField(blank=True, null=True)
    auction_bid_at = models.IntegerField()
    ally_penalty_expiry_time = models.PositiveBigIntegerField()
    ally_penalty_type = models.IntegerField()
    char_penalty_expiry_time = models.PositiveBigIntegerField()
    dissolving_expiry_time = models.PositiveBigIntegerField()
    new_leader_id = models.PositiveIntegerField()

    class Meta:
        managed = False
        db_table = 'clan_data'
        verbose_name = 'clan'
        verbose_name_plural = 'Clans'
