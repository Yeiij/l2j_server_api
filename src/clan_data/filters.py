from django_filters import rest_framework as filters

from .models import ClanData


class ClanDataFilter(filters.FilterSet):
    class Meta:
        model = ClanData
        fields = {
            'clan_name': ['contains', ],
        }
