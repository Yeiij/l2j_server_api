# L2JServer API
API implementation for [L2JServer](https://www.l2jserver.com/)
 
+ [Python 3.9](https://www.python.org/)  
+ [Django 3.1](https://www.djangoproject.com/)  
+ [MariaDB 10.5](https://mariadb.org/)  


## Configurations
### .env File
Create a file named `.env`
```
l2j_server_api/l2j_server_api/.env
```

This is the content
```dotenv
# Environment Configuration, values: dev, prod
DEVELOPMENT=True

# Django Configuration
SECRET_KEY=l2jserverctt&7s*kgonp)z)x_ii12p$!&#m+=or$6+tpkclx!gtmmh%br_
DEBUG=True
ALLOWED_HOSTS="127.0.0.1, localhost"

# Database Configuration
NAME=l2jgs
USER=l2j
PASSWORD=l2jserver2019
HOST=127.0.0.1
PORT=3306
```
## API URL's
ALL THESE URLS ACCEPTS **GET** REQUESTS ONLY  

### Player Related
[TOP **PK** URL](http://127.0.0.1:8000/api/characters/top-pk/) `http://127.0.0.1:8000/api/characters/top-pk/`  
[TOP **PVP** URL](http://127.0.0.1:8000/api/characters/top-pvp/) `http://127.0.0.1:8000/api/characters/top-pvp/`


### Clan Related
[TOP **Level** URL](http://127.0.0.1:8000/api/clan_data/top-level) `http://127.0.0.1:8080/api/clan_data/top-level`  


### Raid Related
[Grandboss **Status** URL](http://127.0.0.1:8000/api/grandboss_data/) `http://127.0.0.1:8000/api/grandboss_data/`  
[Raidboss **Status** URL](http://127.0.0.1:8000/api/raidboss_spawnlist/) `http://127.0.0.1:8000/api/raidboss_spawnlist/`  


----


# Tools

# How to run standalone Tools

1.- Install Python 3.9+  
2.- Install the next libraries  
3.- Check the script and fill those paths as you wish
```commandline
pip install beautifulsoup4
pip install cowsay
pip install lxml
```
4.- Run the script, Where **tool** is the name of the file  
```commandline
python tools/tool.py
```
5.- Done, gl!
